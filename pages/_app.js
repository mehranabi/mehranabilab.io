import React, { useState } from 'react'
import Head from 'next/head'
import { ThemeProvider } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'
import { SnackbarProvider } from 'notistack'

import theme from '../src/theme'

const MyApp = ({ Component, pageProps }) => {
    React.useEffect(() => {
        const jssStyles = document.querySelector('#jss-server-side')
        if (jssStyles) {
            jssStyles.parentElement.removeChild(jssStyles)
        }
    }, [])

    const [darkMode, setDarkMode] = useState(false)
    const toggleDarkMode = () => setDarkMode(!darkMode)

    return (
        <React.Fragment>
            <Head>
                <title>S. Mehran Abghari | Full-Stack Developer</title>
                <meta
                    name="viewport"
                    content="minimum-scale=1, initial-scale=1, width=device-width"
                />
            </Head>
            <ThemeProvider theme={theme(darkMode)}>
                <SnackbarProvider>
                    <CssBaseline />
                    <Component
                        {...pageProps}
                        isDarkMode={darkMode}
                        toggleDarkMode={toggleDarkMode}
                    />
                </SnackbarProvider>
            </ThemeProvider>
        </React.Fragment>
    )
}

export default MyApp
