import React from 'react'

const HomePage = ({ isDarkMode, toggleDarkMode }) => (
    <div style={`background-color: ${isDarkMode ? 'black' : 'white'};`}>
        <h1 style={`color: ${isDarkMode ? 'white' : 'black'}`}>Hello from Mehran :D</h1>
        <button style="margin-top: 1em;">Go {isDarkMode ? 'Light' : 'Dark'} Mode</button>
    </div>
)

export default HomePage
