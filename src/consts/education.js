const education = [
    {
        major: 'Computer Engineering (Bc.s)',
        school: 'Urmia University of Technology',
    },
    {
        major: 'Mathematics & Physics',
        school: 'Rahimi Afshar Highschool (12th grade)',
    },
    {
        major: 'Mathematics & Physics',
        school: 'Derakhshande Sarraf Examplary Highschool (10th & 11th grade)',
    },
]

export default education
