const experience = [
    {
        role: 'Full-Stack Developer',
        company: 'SuperStar Technology Corp.',
        from: '2018-09-01',
        to: 'now',
        description: '(Full-Time)',
    },
    {
        role: 'Mobile Developer',
        company: 'Amoozesh Majazi Daneshgahian',
        from: '2019-06-01',
        to: '2019-11-01',
        description:
            "(Project) Worked as a React Native developer to build Nahad's mobile app for" +
            ' Professors.\nWorked with:\n+ React Native, RN Elements, Redux, Redux Saga,' +
            ' Formik and Axios.',
    },
    {
        role: 'Web Developer',
        company: 'XVA Corp.',
        from: '2019-03-01',
        to: '2020-10-01',
        description:
            '(Part-Time) Worked as a web developer for company, have built 4 versions of the' +
            ' website (Laravel + Semantic UI | WordPress).\n\nManaging a private OpenVPN server' +
            ' for company.',
    },
    {
        role: 'Web App Developer',
        company: 'Aleekhan',
        from: '2019-11-01',
        to: '2020-10-01',
        description:
            '(Part-Time) Worked as a web developer to build a custom ERP software for company using:\n' +
            '+ PHP, Laravel, MySQL, Semantic UI and jQuery.\n\nAlso, worked to built 2 websites for' +
            ' company using WordPress | UIKit.',
    },
    {
        role: 'React Native Intern',
        company: 'Rahnema College',
        from: '2019-06-01',
        to: '2019-09-01',
        description:
            '(Internship) A 7-week internship program at Rahnema College.\n' +
            'I worked in a team of 4 to build an online mobile' +
            ' application with React Native.\nWorked with React' +
            ', React Native, Redux, Redux Saga and WebSocket.\n\n' +
            'Worked in an agile environment with Scrum method.\n\n' +
            'Also learned about Git, TDD, Software Patterns' +
            ', DevOps, CI/CD and Docker.',
    },
    {
        role: 'Freelance Software Developer',
        company: '🏠',
        from: '2015-01-01',
        to: '2020-10-01',
        description:
            'Have worked as a freelance software developer for teams and companies;' +
            ' both as back-end and front-end developer.\n\nHave worked with many technologies' +
            ' as a Front-End developer such as: JavaScript, React.js, Next.js and React Native,' +
            ' also experienced working with popular ui-kits such as Bootstrap, Semantic UI and UIKit.\n\n' +
            "And as a Back-End developer, I've worked with: PHP, Laravel, MySQL and have some experience" +
            ' working with GraphQL, Grpc, and MongoDB.\n In this while I learned many things about' +
            ' APIs, Databases and Data Structures, Securing Web Apps and Authentication' +
            ' methods.\n\nI also worked as a full-time Android developer (with Java) for 2 years.\n\n' +
            'I learned so many things about DevOps, linux server management and things like CI/CD.\n' +
            'And worth mentioning that Git made my life much easier (and my code safer) in these years.',
    },
]

export default experience
