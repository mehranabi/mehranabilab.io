const projects = [
    {
        name: 'SimpMailer',
        description:
            'A mailing system built with Laravel, users can subscribe for mailing list and admins can send them emails. Email templates are managed by Eclipse-Mail and content can be written in MarkDown for rich styling. And its Open-Source.',
        url: 'https://gitlab.com/mehranabi/simpmailer',
    },
    {
        name: 'Anti-Silent',
        description:
            'An Android application written in Java, that lets users to control their mobile phone ringtone mode with pre-defined SMS codes.',
        url: 'https://gitlab.com/mehranabi/anti-silent',
    },
    {
        name: 'myapi.ms',
        description:
            'A React.js showcase app, uses myapi.ms API to fetch WHOIS information for a given domain name or IP address.',
        url: 'https://gitlab.com/mehranabi/myapi-ms',
    },
    {
        name: 'Josephus in CPP',
        description:
            'A CPP app that implements Circular Linked List and then solves Josephus problem using it.',
        url: 'https://gitlab.com/mehranabi/josephus-cpp',
    },
    {
        name: 'Java Linked List',
        description: 'Linked List implementation in Java',
        url: 'https://gitlab.com/mehranabi/linked-list-java',
    },
    {
        name: 'Lestroyer plug-in',
        description:
            'Lestroyer (Laravel + Destroyer) is a simple Laravel plugin that can remove some mandatory files by calling an endpoint.',
        url: 'https://gitlab.com/mehranabi/lestroyer',
    },
    {
        name: 'SimpUMS',
        description:
            'A simple C# + Windows Forms application which can do CRUD on different model in a University. Using MySQL as database.',
        url: 'https://gitlab.com/mehranabi/unive',
    },
]

export default projects
