import React from 'react'
import AtIcon from 'mdi-material-ui/At'
import TelegramIcon from 'mdi-material-ui/Telegram'
import GitlabIcon from 'mdi-material-ui/Gitlab'
import GithubIcon from 'mdi-material-ui/Github'
import TwitterIcon from 'mdi-material-ui/Twitter'
import LinkedinIcon from 'mdi-material-ui/Linkedin'

const socials = [
    {
        name: 'Email',
        icon: <AtIcon />,
        label: 'mehran.ab80@gmail.com',
        link: 'mailto:mehran.ab80@gmail.com',
    },
    {
        name: 'Telegram',
        icon: <TelegramIcon />,
        label: '@ThisIsMehran',
        link: 'https://t.me/thisismehran',
    },
    {
        name: 'Gitlab',
        icon: <GitlabIcon />,
        label: '@mehranabi',
        link: 'https://gitlab.com/mehranabi',
    },
    {
        name: 'Github',
        icon: <GithubIcon />,
        label: '@mehranabi',
        link: 'https://github.com/mehranabi',
    },
    {
        name: 'Twitter',
        icon: <TwitterIcon />,
        label: '@itsmehranabi',
        link: 'https://twitter.com/itsmehranabi',
    },
    {
        name: 'Linkedin',
        icon: <LinkedinIcon />,
        label: 'in/abghari-mehran',
        link: 'https://linkedin.com/in/abghari-mehran',
    },
]

export default socials
