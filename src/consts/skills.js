import React from 'react'
import PhpIcon from 'mdi-material-ui/LanguagePhp'
import LaravelIcon from 'mdi-material-ui/Laravel'
import JsIcon from 'mdi-material-ui/LanguageJavascript'
import ReactIcon from 'mdi-material-ui/React'
import JavaIcon from 'mdi-material-ui/LanguageJava'

const skills = [
    {
        title: 'PHP',
        rating: 4.5,
        icon: <PhpIcon />,
        color: '#474A8A',
    },
    {
        title: 'Laravel',
        rating: 4,
        icon: <LaravelIcon />,
        color: '#F05340',
    },
    {
        title: 'JavaScript',
        rating: 4.5,
        icon: <JsIcon />,
        color: '#F0DB4F',
    },
    {
        title: 'React.js',
        rating: 4,
        icon: <ReactIcon />,
        color: '#61DBFB',
    },
    {
        title: 'React Native',
        rating: 4,
        icon: <ReactIcon />,
        color: '#61DBFB',
    },
    {
        title: 'Next.js',
        rating: 4.5,
        icon: <ReactIcon />,
        color: '#aaa',
    },
    {
        title: 'Java',
        rating: 4,
        icon: <JavaIcon />,
        color: '#f89820',
    },
]

export const secondary = [
    'HTML',
    'CSS',
    'SQL',
    'MySQL',
    'Git',
    'Git Flow',
    'Linux',
    'Android',
    'Redux.js',
    'Python',
    'Arduino',
    'Raspberry Pi',
    'Scrum',
    'Leadership',
]

export default skills
