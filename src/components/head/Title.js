import React, { useState } from 'react'
import Fade from '@material-ui/core/Fade'
import Typography from '@material-ui/core/Typography'

const Title = () => {
    const [isHovered, setHovered] = useState(false)

    const hover = () => setHovered(true)
    const unhover = () => setHovered(false)

    return (
        <div>
            <Typography
                variant="h6"
                onMouseEnter={hover}
                onMouseLeave={unhover}
            >
                S
                <Fade in={isHovered} mountOnEnter unmountOnExit>
                    <Typography variant="h6" display="inline">
                        eyed
                    </Typography>
                </Fade>{' '}
                Mehran Abghari |{' '}
                <Typography variant="body1" display="inline">
                    Full-Stack Developer
                </Typography>
            </Typography>
        </div>
    )
}

export default Title
