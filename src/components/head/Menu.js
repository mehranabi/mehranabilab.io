import React from 'react'

import Navigation from '../common/Navigation'

const Menu = ({ navClasses }) => (
    <div>
        <Navigation title="About Me" btnClasses={navClasses} href="#about" />
        <Navigation title="Skills" btnClasses={navClasses} href="#skills" />
        <Navigation
            title="Experiences"
            btnClasses={navClasses}
            href="#experiences"
        />
        <Navigation
            title="Contact Me"
            btnClasses={navClasses}
            href="#contact"
        />
    </div>
)

export default Menu
