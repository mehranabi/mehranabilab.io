import React from 'react'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Switch from '@material-ui/core/Switch'
import Tooltip from '@material-ui/core/Tooltip'
import yellow from '@material-ui/core/colors/yellow'

import Title from './Title'
import { Elevated } from '../../appbars'

const DarkModeSwitch = withStyles((theme) => {
    const isDark = theme.palette.type === 'dark'
    return {
        thumb: {
            backgroundColor: isDark ? yellow.A100 : '#000',
            backgroundImage: isDark ? 'url(/sun.png)' : 'url(/moon.png)',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            backgroundSize: 'cover',
        },
    }
})(Switch)

const useStyles = makeStyles((theme) => ({
    filler: {
        flexGrow: 1,
    },
    nav: {
        color: theme.palette.getContrastText(theme.palette.primary.main),
    },
    switch: {
        marginLeft: theme.spacing(3),
    },
}))

const Header = ({ isDark, onDarkToggle }) => {
    const classes = useStyles()

    return (
        <Elevated>
            <AppBar color="primary" position="sticky">
                <Toolbar>
                    <Title />
                    <div className={classes.filler} />
                    <Tooltip title="Toggle Dark Mode">
                        <DarkModeSwitch
                            className={classes.switch}
                            checked={isDark}
                            onChange={onDarkToggle}
                            color="secondary"
                        />
                    </Tooltip>
                </Toolbar>
            </AppBar>
        </Elevated>
    )
}

export default Header
