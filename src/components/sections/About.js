import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import Collapse from '@material-ui/core/Collapse'
import Tooltip from '@material-ui/core/Tooltip'
import Link from '@material-ui/core/Link'
import Avatar from '@material-ui/core/Avatar'
import ExpandIcon from '@material-ui/icons/ExpandMoreRounded'
import CollapseIcon from '@material-ui/icons/ExpandLessRounded'
import Typist from 'react-typist'
import clsx from 'clsx'

const useStyles = makeStyles((theme) => ({
    container: {
        paddingTop: theme.spacing(5),
        paddingBottom: theme.spacing(5),
    },
    button: {
        marginTop: theme.spacing(2),
    },
    tooltiper: {
        textDecoration: 'underline',
        textDecorationStyle: 'dotted',
    },
    avatar: {
        width: 300,
        height: 300,
    },
    avatar2: {
        marginTop: theme.spacing(5),
    },
}))

const AboutSection = () => {
    const classes = useStyles()

    const [showMore, setShowMore] = useState(false)

    const openMore = () => setShowMore(true)
    const closeMore = () => setShowMore(false)

    return (
        <div id="about">
            <Container className={classes.container}>
                <Grid container spacing={3}>
                    <Grid item xs={12} md={7}>
                        <Typography variant="h4">Hi there!</Typography>
                        <Typography variant="h4" color="primary">
                            I'm Seyed Mehran Abghari!
                        </Typography>
                        <Typography variant="h6" gutterBottom>
                            <Typist
                                avgTypingDelay={50}
                                cursor={{
                                    hideWhenDone: true,
                                }}
                            >
                                <br />A <b>Full-Stack Developer</b>,{' '}
                                <Typist.Delay ms={500} />
                                with 5+ years of experience,{' '}
                                <Typist.Delay ms={500} />
                                who loves working with <b>PHP</b> and{' '}
                                <b>JavaScript</b>
                                <Typist.Backspace count={18} delay={1000} />
                                <b>Laravel</b> and <b>React.js</b>
                                <Typist.Backspace count={8} delay={1000} />
                                <b>React Native</b>
                                <Typist.Backspace count={12} delay={1000} />
                                <b>Next.js</b>
                                <Typist.Delay ms={1000} />, <b>React.js</b> and{' '}
                                <b>React Native</b> 🙂
                            </Typist>
                        </Typography>
                        <Collapse in={showMore} mountOnEnter unmountOnExit>
                            <br />
                            <Typography variant="body1">
                                Thanks for being interested in my story 😀
                            </Typography>
                            <br />
                            <Typography variant="body1">
                                <b>Mehran</b> in one line:
                            </Typography>
                            <Typography variant="body1">
                                💻 A computer lover, who is passionate to learn
                                and experience new things and to meet new
                                people.
                            </Typography>
                            <br />
                            <Typography variant="body1">
                                I'm a{' '}
                                <Tooltip title="was born in 26 Aug 2001">
                                    <span className={classes.tooltiper}>
                                        19 years old
                                    </span>
                                </Tooltip>{' '}
                                student from Iran, currently living in{' '}
                                <Tooltip title="A beautiful city in north-west of Iran">
                                    <span className={classes.tooltiper}>
                                        Urmia
                                    </span>
                                </Tooltip>{' '}
                                and studying Computer Engineering at{' '}
                                <Tooltip title="Urmia University of Technology">
                                    <span className={classes.tooltiper}>
                                        UUT
                                    </span>
                                </Tooltip>{' '}
                                and beside studying I work as a software
                                developer.
                            </Typography>
                            <br />
                            <Typography variant="body1">
                                I like movies,{' '}
                                <Tooltip title="Doesn't mean that I don't care about personal health, I do exercise regularly :)">
                                    <span className={classes.tooltiper}>
                                        not a big fan of sports
                                    </span>
                                </Tooltip>
                                ; but love rain 🌧️, snow ❄, and nature 🌲.
                            </Typography>
                            <br />
                            <Typography variant="body1">
                                My programming career began 6 years ago, when I
                                started self-learning Java for mobile
                                development. I continued Android Development for
                                2 years and published some applications on
                                mobile app stores (
                                <Link
                                    href="https://myket.ir/developer/dev-27189"
                                    target="_blank"
                                    rel="nofollow"
                                >
                                    here for example
                                </Link>
                                ).
                            </Typography>
                            <Typography variant="body1">
                                I started working with PHP and JavaScript for
                                web development a bit later, and I developed
                                many websites and web-apps in this while.
                            </Typography>
                            <br />
                            <Typography variant="body1">
                                With having knowledge of JS, I started working
                                with React.js and React Native for web and
                                hybrid mobile application development. Also as I
                                knew some PHP and had worked with Laravel, I
                                could built almost any web application I wanted
                                to.
                            </Typography>
                            <Typography variant="body1">
                                In this while, I learnt about Git, Databases and
                                Data Structures, API Designing, Security
                                guidelines, Authorization and Auth methods and
                                many many more.
                            </Typography>
                            <br />
                            <Typography variant="body1">
                                In these years of being obsessed with computers,
                                I coded in many languages including: Java, PHP,
                                JavaScript, Python, Golang, C/Cpp and C# and bit
                                of Kotlin and Dart. And still my favorite is
                                Java, although I use it only for{' '}
                                <Link
                                    href="https://quera.ir/profile/mehran.ab80"
                                    target="_blank"
                                    rel="nofollow"
                                >
                                    Quera
                                </Link>{' '}
                                coding challenges these days.
                            </Typography>
                            <br />
                            <Typography variant="body1">
                                I have a very strong interest in AI, especially
                                Machine Learning and I'm trying to do some
                                self-studying and I believe it's gonna be the
                                feature of humanity.
                            </Typography>
                            <br />
                            <Typography varoant="body1">
                                Beside these software things, I like helping my
                                dad with electronic and hardware stuff, such as
                                building systems using Arduino, Raspberry Pi and
                                etc.
                            </Typography>
                        </Collapse>
                        <Button
                            variant="contained"
                            color="secondary"
                            onClick={showMore ? closeMore : openMore}
                            endIcon={
                                showMore ? <CollapseIcon /> : <ExpandIcon />
                            }
                            className={classes.button}
                        >
                            {showMore ? 'Collapse' : 'Read More'}
                        </Button>
                    </Grid>
                    <Grid
                        item
                        xs={12}
                        md={5}
                        container
                        justify="center"
                        alignItems="center"
                        direction="column"
                    >
                        <Avatar
                            alt="Seyed Mehran Abghari - Full Stack Software Developer"
                            src="/mehran-abghari.jpg"
                            className={classes.avatar}
                        />
                        <Collapse in={showMore} mountOnEnter unmountOnExit>
                            <Avatar
                                alt="Seyed Mehran Abghari - Full Stack Software Developer"
                                src="/mehran-abghari-2.jpg"
                                className={clsx(
                                    classes.avatar,
                                    classes.avatar2
                                )}
                            />
                        </Collapse>
                    </Grid>
                </Grid>
            </Container>
        </div>
    )
}

export default AboutSection
