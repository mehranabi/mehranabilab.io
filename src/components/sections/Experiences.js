import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'

import Experience from '../items/Experience'
import EXPERIENCES from '../../consts/experience'

const useStyles = makeStyles((theme) => ({
    container: {
        paddingTop: theme.spacing(5),
        paddingBottom: theme.spacing(5),
    },
    content: {
        marginTop: theme.spacing(5),
    },
}))

const ExperiencesSection = () => {
    const classes = useStyles()

    return (
        <div id="experiences">
            <Container className={classes.container}>
                <Typography
                    gutterBottom
                    variant="h4"
                    color="primary"
                    align="center"
                >
                    My Experiences
                </Typography>
                <div className={classes.content}>
                    {EXPERIENCES.map((exp, i) => (
                        <Experience key={i} {...exp} />
                    ))}
                </div>
            </Container>
        </div>
    )
}

export default ExperiencesSection
