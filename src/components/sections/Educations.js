import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import Timeline from '@material-ui/lab/Timeline'

import Education from '../items/Education'
import EDUCATIONS from '../../consts/education'

const useStyles = makeStyles((theme) => ({
    container: {
        paddingTop: theme.spacing(5),
        paddingBottom: theme.spacing(5),
    },
    content: {
        marginTop: theme.spacing(5),
    },
}))

const EducationsSection = () => {
    const classes = useStyles()

    return (
        <div id="experiences">
            <Container className={classes.container}>
                <Typography
                    gutterBottom
                    variant="h4"
                    color="primary"
                    align="center"
                >
                    My Education
                </Typography>
                <div className={classes.content}>
                    <Timeline align="alternate">
                        {EDUCATIONS.map((education, index) => (
                            <Education key={index} {...education} />
                        ))}
                    </Timeline>
                </div>
            </Container>
        </div>
    )
}

export default EducationsSection
