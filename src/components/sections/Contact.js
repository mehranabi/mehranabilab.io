import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import { useSnackbar } from 'notistack'

import Social from '../items/Social'
import SOCIALS from '../../consts/socials'
import ContactForm from '../forms/ContactForm'
import { ContactFormRequest } from '../../services/Network'

const useStyles = makeStyles((theme) => ({
    container: {
        marginTop: theme.spacing(5),
        marginBottom: theme.spacing(5),
    },
    content: {
        marginTop: theme.spacing(5),
    },
}))

const ContactSection = () => {
    const classes = useStyles()
    const { enqueueSnackbar } = useSnackbar()

    const onFormSubmit = async (values, actions) => {
        actions.setSubmitting(true)
        const response = await ContactFormRequest({
            ...values,
            '_email.from': values.name,
            '_email.subject': 'Website Contact Form',
        })
        if (response.message) {
            enqueueSnackbar(response.message, { variant: 'success' })
        } else {
            enqueueSnackbar(response.error, { variant: 'error' })
        }
    }

    return (
        <div id="contact">
            <Container className={classes.container}>
                <Typography
                    gutterBottom
                    variant="h4"
                    color="primary"
                    align="center"
                >
                    Contact me
                </Typography>
                <Grid container spacing={5} className={classes.content}>
                    <Grid item xs={12} md={6}>
                        <ContactForm onSubmit={onFormSubmit} />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        {SOCIALS.map((social) => (
                            <Social key={social.name} {...social} />
                        ))}
                    </Grid>
                </Grid>
            </Container>
        </div>
    )
}

export default ContactSection
