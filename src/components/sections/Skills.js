import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Chip from '@material-ui/core/Chip'

import Skill from '../items/Skill'
import SKILLS, { secondary as SECOND_SKILLS } from '../../consts/skills'

const useStyles = makeStyles((theme) => ({
    container: {
        paddingTop: theme.spacing(5),
        paddingBottom: theme.spacing(5),
    },
    grid: {
        marginTop: theme.spacing(5),
    },
    chip: {
        marginLeft: theme.spacing(0.5),
        marginTop: theme.spacing(0.5),
    },
}))

const SkillsSection = () => {
    const classes = useStyles()

    return (
        <div className={classes.root} id="skills">
            <Container className={classes.container}>
                <Typography
                    gutterBottom
                    variant="h4"
                    color="primary"
                    align="center"
                >
                    My Skills
                </Typography>
                <Grid container spacing={3} className={classes.grid}>
                    {SKILLS.map((skill) => (
                        <Skill key={skill.title} {...skill} />
                    ))}
                    <Grid item xs={12}>
                        <Card>
                            <CardContent>
                                {SECOND_SKILLS.map((t) => (
                                    <Chip
                                        key={t}
                                        color="primary"
                                        label={t}
                                        className={classes.chip}
                                    />
                                ))}
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
            </Container>
        </div>
    )
}

export default SkillsSection
