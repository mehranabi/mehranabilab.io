import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import Link from '@material-ui/core/Link'

import Project from '../items/Project'
import PROJECTS from '../../consts/projects'

const useStyles = makeStyles((theme) => ({
    container: {
        marginTop: theme.spacing(5),
        marginBottom: theme.spacing(5),
    },
    content: {
        marginTop: theme.spacing(5),
    },
    buttons: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: theme.spacing(5),
    },
    buttonLeft: {
        marginLeft: theme.spacing(2),
    },
    buttonRight: {
        marginRight: theme.spacing(2),
    },
}))

const ProjectsSection = () => {
    const classes = useStyles()

    return (
        <div id="projects">
            <Container className={classes.container}>
                <Typography
                    gutterBottom
                    variant="h4"
                    color="primary"
                    align="center"
                >
                    My Projects
                </Typography>
                <Grid
                    container
                    spacing={2}
                    alignItems="stretch"
                    className={classes.content}
                >
                    {PROJECTS.map((project) => (
                        <Grid key={project.name} item xs={12} md={4}>
                            <Project {...project} />
                        </Grid>
                    ))}
                </Grid>
                <div className={classes.buttons}>
                    <Typography align="center" variant="h6" gutterBottom>
                        And everything else at
                    </Typography>
                    <div>
                        <Link
                            href="https://github.com/mehranabi"
                            target="_blank"
                            rel="nofollow"
                        >
                            <Button
                                className={classes.buttonRight}
                                variant="outlined"
                                color="primary"
                            >
                                Github
                            </Button>
                        </Link>
                        <span>AND</span>
                        <Link
                            href="https://gitlab.com/mehranabi"
                            target="_blank"
                            rel="nofollow"
                        >
                            <Button
                                className={classes.buttonLeft}
                                variant="outlined"
                                color="primary"
                            >
                                Gitlab
                            </Button>
                        </Link>
                    </div>
                </div>
            </Container>
        </div>
    )
}

export default ProjectsSection
