import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import AccordionSummary from '@material-ui/core/AccordionSummary'
import ExpandIcon from '@material-ui/icons/ExpandMoreRounded'
import Typography from '@material-ui/core/Typography'
import AccordionDetails from '@material-ui/core/AccordionDetails'
import Accordion from '@material-ui/core/Accordion'
import moment from 'moment'

const useStyles = makeStyles({
    title: {
        flexGrow: 1,
    },
})

const ExperienceItem = ({ role, company, from, to, description }) => {
    const classes = useStyles()

    return (
        <Accordion>
            <AccordionSummary expandIcon={<ExpandIcon />}>
                <Typography variant="body1" className={classes.title}>
                    <b>{role}</b> at <b>{company}</b>
                </Typography>
                <Typography variant="subtitle2" color="textSecondary">
                    {moment(from).format('MMM YYYY')} to{' '}
                    {to === 'now'
                        ? 'Present'
                        : moment(to).format('MMM YYYY') +
                          ' (' +
                          moment(to).diff(moment(from), 'months') +
                          ' months)'}
                </Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Typography variant="body1">
                    {description.split('\n').map((s, i) => (
                        <React.Fragment key={i}>
                            {s}
                            <br />
                        </React.Fragment>
                    ))}
                </Typography>
            </AccordionDetails>
        </Accordion>
    )
}

export default ExperienceItem
