import React from 'react'
import Typography from '@material-ui/core/Typography'
import TimelineOppositeContent from '@material-ui/lab/TimelineOppositeContent'
import TimelineSeparator from '@material-ui/lab/TimelineSeparator'
import TimelineDot from '@material-ui/lab/TimelineDot'
import TimelineConnector from '@material-ui/lab/TimelineConnector'
import TimelineContent from '@material-ui/lab/TimelineContent'
import TimelineItem from '@material-ui/lab/TimelineItem'

const EducationItem = ({ major, school }) => (
    <TimelineItem>
        <TimelineOppositeContent>
            <Typography color="textSecondary">{major}</Typography>
        </TimelineOppositeContent>
        <TimelineSeparator>
            <TimelineDot color="secondary" />
            <TimelineConnector />
        </TimelineSeparator>
        <TimelineContent>
            <Typography>
                <b>{school}</b>
            </Typography>
        </TimelineContent>
    </TimelineItem>
)

export default EducationItem
