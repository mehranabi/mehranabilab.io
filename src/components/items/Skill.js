import React, { cloneElement } from 'react'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import Rating from '@material-ui/lab/Rating'
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'

const useStyles = makeStyles((theme) => ({
    card: {
        padding: theme.spacing(2, 5),
        transition: 'transform 0.5s',
        '&:hover': {
            transform: 'translateY(-10%) scale(1.1)',
        },
    },
}))

const SkillItem = ({ title, icon, color, rating }) => {
    const classes = useStyles()

    const StyledRating = withStyles((theme) => ({
        iconFilled: {
            color,
        },
        iconEmpty: {
            color: theme.palette.getContrastText(
                theme.palette.background.paper
            ),
        },
    }))(Rating)

    return (
        <Grid
            item
            container
            direction="column"
            alignItems="center"
            xs={12}
            sm={6}
            md={4}
            lg={3}
            key={title}
        >
            <Card className={classes.card}>
                <CardContent>
                    <Typography variant="h6">{title}</Typography>
                    <StyledRating
                        precision={0.5}
                        value={rating}
                        readOnly
                        icon={cloneElement(icon, {
                            fontSize: 'inherit',
                        })}
                        size="large"
                    />
                </CardContent>
            </Card>
        </Grid>
    )
}

export default SkillItem
