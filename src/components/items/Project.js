import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import MuiLink from '@material-ui/core/Link'

const useStyles = makeStyles({
    card: {
        height: '100%',
    },
    actionArea: {
        height: '100%',
    },
})

const ProjectItem = ({ name, description, url }) => {
    const classes = useStyles()

    return (
        <Card className={classes.card}>
            <MuiLink href={url} underline="none" target="_blank" rel="nofollow">
                <CardActionArea className={classes.actionArea}>
                    <CardContent>
                        <Typography variant="h6" color="secondary" gutterBottom>
                            {name}
                        </Typography>
                        <Typography variant="body1" color="textSecondary">
                            {description}
                        </Typography>
                    </CardContent>
                </CardActionArea>
            </MuiLink>
        </Card>
    )
}

export default ProjectItem
