import React, { cloneElement } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Link from '@material-ui/core/Link'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Tooltip from '@material-ui/core/Tooltip'

const useStyles = makeStyles((theme) => ({
    item: {
        marginTop: theme.spacing(1),
    },
    icon: {
        marginRight: theme.spacing(2),
    },
}))

const SocialItem = ({ label, icon, link, name }) => {
    const classes = useStyles()

    return (
        <Grid container alignItems="center" className={classes.item}>
            <Tooltip title={name}>
                {cloneElement(icon, {
                    fontSize: 'large',
                    className: classes.icon,
                    color: 'secondary',
                })}
            </Tooltip>
            <Link href={link} target="_blank">
                <Typography variant="body1">{label}</Typography>
            </Link>
        </Grid>
    )
}

export default SocialItem
