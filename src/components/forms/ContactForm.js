import React from 'react'
import Grid from '@material-ui/core/Grid'
import CircularProgress from '@material-ui/core/CircularProgress'
import Button from '@material-ui/core/Button'
import InputAdornment from '@material-ui/core/InputAdornment'
import SendIcon from '@material-ui/icons/SendRounded'
import FaceIcon from '@material-ui/icons/FaceRounded'
import MailIcon from '@material-ui/icons/MailOutlineRounded'
import { Field, Form, Formik } from 'formik'
import { TextField } from 'formik-material-ui'
import { object, string } from 'yup'

import strings from '../../strings'

const NameStrings = strings.validation('Name')
const EmailStrings = strings.validation('Email Address')
const MessageStrings = strings.validation('Message')

const ValidationSchema = object().shape({
    name: string()
        .required(NameStrings.required)
        .min(3, NameStrings.min)
        .max(20, NameStrings.max),
    email: string()
        .required(EmailStrings.required)
        .email(EmailStrings.email)
        .max(100, EmailStrings.max),
    message: string()
        .required(MessageStrings.required)
        .min(10, MessageStrings.min)
        .max(1000, MessageStrings.max),
})

const ContactForm = ({ onSubmit }) => {
    return (
        <Formik
            initialValues={{
                name: '',
                email: '',
                message: '',
            }}
            validationSchema={ValidationSchema}
            onSubmit={onSubmit}
        >
            {({ isSubmitting, handleSubmit }) => (
                <Form>
                    <input
                        type="checkbox"
                        name="_IM_FROM_REAL_WEBSITE_"
                        style={{ display: 'none' }}
                        tabIndex="-1"
                        autoComplete="off"
                    />
                    <Grid container spacing={2}>
                        <Grid item xs={12} md={6}>
                            <Field
                                component={TextField}
                                name="name"
                                label="Name"
                                placeholder="Your full name"
                                variant="filled"
                                fullWidth
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <FaceIcon />
                                        </InputAdornment>
                                    ),
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <Field
                                component={TextField}
                                name="email"
                                label="Email Address"
                                type="email"
                                variant="filled"
                                fullWidth
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <MailIcon />
                                        </InputAdornment>
                                    ),
                                }}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Field
                                component={TextField}
                                name="message"
                                label="Message"
                                variant="filled"
                                fullWidth
                                multiline
                                rows={8}
                            />
                        </Grid>
                        <Grid item xs={12} container justify="center">
                            {isSubmitting ? (
                                <CircularProgress color="primary" />
                            ) : (
                                <Button
                                    variant="contained"
                                    color="primary"
                                    endIcon={<SendIcon />}
                                    onClick={handleSubmit}
                                >
                                    Submit
                                </Button>
                            )}
                        </Grid>
                    </Grid>
                </Form>
            )}
        </Formik>
    )
}

export default ContactForm
