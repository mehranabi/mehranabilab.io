import React from 'react'
import Link from 'next/link'
import MuiLink from '@material-ui/core/Link'
import Button from '@material-ui/core/Button'

const Navigation = ({ title, btnClasses, ...props }) => (
    <Link shallow passHref {...props}>
        <MuiLink underline="none">
            <Button className={btnClasses}>{title}</Button>
        </MuiLink>
    </Link>
)

export default Navigation
