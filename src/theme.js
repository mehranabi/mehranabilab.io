import { createMuiTheme } from '@material-ui/core/styles'

const theme = (dark) =>
    createMuiTheme({
        palette: {
            type: dark ? 'dark' : 'light',
        },
    })

export default theme
