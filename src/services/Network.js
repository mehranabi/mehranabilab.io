import Axios from 'axios'

export const ContactFormRequest = async (data) => {
    try {
        const resp = await Axios.post(
            'https://submit-form.com/YdhCNBg9XdXPOp7vvESgT',
            data,
            {
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                },
            }
        )

        return {
            message: 'Your message has been sent. Thanks!',
        }
    } catch (e) {
        return {
            error: 'Cannot send message to Spark!',
        }
    }
}
