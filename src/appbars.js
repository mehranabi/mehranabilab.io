import React, { cloneElement } from 'react'
import useScrollTrigger from '@material-ui/core/useScrollTrigger'

export const Elevated = ({ children, window }) => {
    const trigger = useScrollTrigger({
        disableHysteresis: true,
        threshold: 0,
        target: window ? window() : undefined,
    })

    return cloneElement(children, {
        elevation: trigger ? 4 : 0,
    })
}
