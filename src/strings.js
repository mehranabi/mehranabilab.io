const strings = {
    validation: (field) => ({
        required: field + ' is required!',
        min: field + ' must be at least ${min} characters!',
        max: field + ' cannot be longer than ${max} characters!',
        email: field + ' must be a valid email address!',
    }),
}

export default strings
